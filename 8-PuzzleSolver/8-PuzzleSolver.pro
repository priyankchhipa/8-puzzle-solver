#-------------------------------------------------
#
# Project created by QtCreator 2011-11-15T04:23:25
#
#-------------------------------------------------

QT       += core gui

TARGET = 8-PuzzleSolver
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    a.cpp \
    ida.cpp

HEADERS  += mainwindow.h \
    a.h \
    ida.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
