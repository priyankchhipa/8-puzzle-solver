/*
 *  main.cpp
 *  
 *
 *  Created by Devendra Chaplot on 10/10/11.
 *  Copyright 2011 IIT Bombay. All rights reserved.
 *
 */


#include "searchTree.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>
#include <algorithm>
#include <time.h>
using namespace std;

int main() {
    char holdExecutionWindowOpen;
    clock_t start, finish, total;
    start = clock();
	
    searchTree b;
    b.build();
	
    finish = clock();
    total = finish - start;
    cout << "time: " << total << " msecs" << endl;
    return 0;
}