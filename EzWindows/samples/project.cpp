#include <iostream>
#include "ezwin.h"
#include <assert.h>
#include "bitmap.h"
//#include<string>
#include<time.h>
#include<stdlib.h>
#include<stdio.h>
//#include<randint.h>
//#include "rect.h"
#include "Alert.h"

SimpleWindow puzzle("8-Puzzle Solver",6.7,14);

BitMap a1(puzzle), a2(puzzle), a3(puzzle), a4(puzzle), a5(puzzle), a6(puzzle), a7(puzzle), a8(puzzle), a9(puzzle);
BitMap pic(puzzle);
int blank;
int array[9];
float x[3];
float y[3];

void movemybmp(BitMap &Image, int dir, Position &pos);
float getx(int a);
float gety(int a);
int MouseClickEvent(const Position &MousePosition);
void BmpMove(BitMap &Image,const Position &Start,const Position &End);

void Load_BitMap(BitMap &Image,const Position &Pos)
{
	assert(Image.GetStatus() == BitMapOkay);
	Image.SetPosition(Pos);	
	Image.Draw();
}

int ApiMain()
{
	puzzle.Open();
	assert(puzzle.GetStatus() == WindowOpen);
	a1.Load("a1.xpm");
	a2.Load("a2.xpm");
	a3.Load("a3.xpm");
	a4.Load("a4.xpm");
	a5.Load("a5.xpm");
	a6.Load("a6.xpm");
	a7.Load("a7.xpm");
	a8.Load("a8.xpm");
	a9.Load("a9.xpm");
	pic.Load("puzzlepic.xpm");
	x[0] = 1.0;
	x[1] = 1.0 + a1.GetWidth();
	x[2] = 1.0 + 2*(a1.GetWidth());
	y[0] = 8.0;
	y[1] = 8.0 + a1.GetHeight();
	y[2] = 8.0 + 2*(a1.GetHeight());
	Load_BitMap(a1, Position(x[0],y[0]));
	Load_BitMap(a2, Position(x[1],y[0]));
	Load_BitMap(a3, Position(x[2],y[0]));
	Load_BitMap(a4, Position(x[0],y[1]));
	Load_BitMap(a5, Position(x[1],y[1]));
	Load_BitMap(a6, Position(x[2],y[1]));
	Load_BitMap(a7, Position(x[0],y[2]));
	Load_BitMap(a8, Position(x[1],y[2]));
	Load_BitMap(pic, Position(1.0,1.0));
	Load_BitMap(a9, Position(x[2],y[2]));

	puzzle.SetMouseClickCallback(MouseClickEvent);

	for(int i = 0; i < 8; i++)
	{
		array[i] = i;
	}
	
	blank = 8;
	usleep(100000);
	Position pos1;
	pos1 = Position(x[1],y[2]);
//	movemybmp(a8, 4, pos1);
	return 0;
}

void BmpMove(BitMap &Image,const Position &Start,const Position &End){
    float StartX=Start.GetXDistance();
    float StartY=Start.GetYDistance();
    float EndX=End.GetXDistance();
    float EndY=End.GetYDistance();
    Position Present=Start, PhotoPosition;
	int Steps = 700;
    for(int k=0;k < Steps;k++){
        PhotoPosition = Present;// + Position(-0.5*Image.GetWidth(),-0.5*Image.GetHeight());
        Image.SetPosition(PhotoPosition);
	    Image.Draw();
	    Present = Present + Position((EndX-StartX)/Steps,(EndY-StartY)/Steps);
	    usleep(100);
    }
    putchar(10);
}

void movemybmp(BitMap &Image, int dir, Position &pos)
{
		Position pos2;
		switch(dir)
		{
			case 1 :
				pos2.SetYDistance(pos.GetYDistance() - Image.GetHeight());
				pos2.SetXDistance(pos.GetXDistance());
				//pos.SetYDistance(pos.GetYDistance() - Image.GetHeight()/2000);
				break;
			case 2 :
				pos2.SetYDistance(pos.GetYDistance()); 
				pos2.SetXDistance(pos.GetXDistance() - Image.GetWidth());
				//pos.SetXDistance(pos.GetXDistance() - Image.GetWidth()/2000);
				break;
			case 3 :
				pos2.SetYDistance(pos.GetYDistance() + Image.GetHeight());
				pos2.SetXDistance(pos.GetXDistance());
				//pos.SetYDistance(pos.GetYDistance() + Image.GetHeight()/2000);
				break;
			case 4 :
				pos2.SetYDistance(pos.GetYDistance()); 
				pos2.SetXDistance(pos.GetXDistance() + Image.GetWidth());
				//pos.SetXDistance(pos.GetXDistance() + Image.GetWidth()/2000);
				break;
			default :
				cout<<"error";
				break;
		}
		BmpMove(Image,pos,pos2);
}

int MouseClickEvent(const Position &MousePosition)
{
//    MouseClick=MousePosition;
    int dif;
    
    //if 1 is clicked
    
	if(a1.IsInside(MousePosition))
	{
		//cout<<"if 1 is clicked\t";
		float posx = getx(array[0]);
		float posy = gety(array[0]);
		dif = array[0] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a1, 1, mypos1);
				array[0] = array[0] -3;
				blank = blank + 3;
				
			}
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a1, 2, mypos1);
				array[0] = array[0] - 1;
				blank = blank + 1;
				
			}
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a1, 4, mypos1);
				array[0] = array[0] + 1;
				blank = blank - 1;
				
			}
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a1, 3, mypos1);
				array[0] = array[0] + 3;
				blank = blank - 3;
				
			}
		//cout<<array[0]<<"\t"<<blank<<endl;
	}
	
	//if 2 is clicked
	
    else if(a2.IsInside(MousePosition))
    {
		//cout<<"if 2 is clicked\t";
		float posx = getx(array[1]);
		float posy = gety(array[1]);
		dif = array[1] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a2, 1, mypos1);
				array[1] = array[1] - 3;
				blank = blank + 3;
				
			}
			
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a2, 2, mypos1);
				array[1] = array[1] - 1;
				blank = blank + 1;
				
			}
			
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a2, 4, mypos1);
				array[1] = array[1] + 1;
				blank = blank - 1;
				
			}
			
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a2, 3, mypos1);
				array[1] = array[1] +3;
				blank = blank -3;
				
			}
		//cout<<array[1]<<"\t"<<blank<<endl;
	}
	
	//if 3 is clicked
	
    else if(a3.IsInside(MousePosition))
    {
		//cout<<"if 3 is clicked\t";
		float posx = getx(array[2]);
		float posy = gety(array[2]);
		dif = array[2] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a3, 1, mypos1);
				array[2] = array[2] - 3;
				blank = blank + 3;
				
			}
			
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a3, 2, mypos1);
				array[2] = array[2] - 1;
				blank = blank + 1;
				
			}
			
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a3, 4, mypos1);
				array[2] = array[2] + 1;
				blank = blank - 1;
				
			}
			
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a3, 3, mypos1);
				array[2] = array[2] + 3;
				blank = blank - 3;
				
			}
		//cout<<array[2]<<"\t"<<blank<<endl;
	}
	
	//if 4 is clicked
    
    else if(a4.IsInside(MousePosition))
    {
		//cout<<"if 4 is clicked\t";
		float posx = getx(array[3]);
		float posy = gety(array[3]);
		dif = array[3] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a4, 1, mypos1);
				array[3] = array[3] - 3;
				blank = blank + 3;
				
			}
			
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a4, 2, mypos1);
				array[3] = array[3] - 1;
				blank = blank + 1;
				
			}
			
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a4, 4, mypos1);
				array[3] = array[3] + 1;
				blank = blank - 1;
				
			}
			
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a4, 3, mypos1);
				array[3] = array[3] + 3;
				blank = blank - 3;
				
			}
		//cout<<array[3]<<"\t"<<blank<<endl;
	}
	
	//if 5 is clicked
	
    else if(a5.IsInside(MousePosition))
    {
		//cout<<"if 5 is clicked\t";
		float posx = getx(array[4]);
		float posy = gety(array[4]);
		dif = array[4] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a5, 1, mypos1);
				array[4] = array[4] - 3;
				blank = blank + 3;
				
			}
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a5, 2, mypos1);
				array[4] = array[4] - 1;
				blank = blank + 1;
				
			}
			
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a5, 4, mypos1);
				array[4] = array[4] + 1;
				blank = blank - 1;
				
			}
			
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a5, 3, mypos1);
				array[4] = array[4] + 3;
				blank = blank - 3;
				
			}
		//cout<<array[4]<<"\t"<<blank<<endl;
	}
	
	//if 6 is clicked
	
    else if(a6.IsInside(MousePosition))
    {
		//cout<<"if 6 is clicked\t";
		float posx = getx(array[5]);
		float posy = gety(array[5]);
		dif = array[5] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a6, 1, mypos1);
				array[5] = array[5] - 3;
				blank = blank + 3;
				
			}
			
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a6, 2, mypos1);
				array[5] = array[5] - 1;
				blank = blank + 1;
				
			}
			
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a6, 4, mypos1);
				array[5] = array[5] + 1;
				blank = blank - 1;
				
			}
			
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a6, 3, mypos1);
				array[5] = array[5] + 3;
				blank = blank - 3;
				
			}
		//cout<<array[5]<<"\t"<<blank<<endl;
	}
	
	//if 7 is clicked
	
    else if(a7.IsInside(MousePosition))
    {
    	//cout<<"if 7 is clicked\t";
		float posx = getx(array[6]);
		float posy = gety(array[6]);
		dif = array[6] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a7, 1, mypos1);
				array[6] = array[6] - 3;
				blank = blank + 3;
				
			}
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a7, 2, mypos1);
				array[6] = array[6] - 1;
				blank = blank + 1;
				
			}
			
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a7, 4, mypos1);
				array[6] = array[6] + 1;
				blank = blank - 1;
				
			}
			
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a7, 3, mypos1);
				array[6] = array[6] + 3;
				blank = blank - 3;
				
			}
		//cout<<array[6]<<"\t"<<blank<<endl;
	}
	
	//if 8 is clicked
	
    else if(a8.IsInside(MousePosition))
    {
    	//cout<<"if 8 is clicked\t";
		float posx = getx(array[7]);
		float posy = gety(array[7]);
		dif = array[7] - blank;
		if (dif == 3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a8, 1, mypos1);
				array[7] = array[7] - 3;
				blank = blank + 3;
				
			}
			
		else if (dif == 1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a8, 2, mypos1);
				array[7] = array[7] - 1;
				blank = blank + 1;
				
			}
			
		else if (dif == -1)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a8, 4, mypos1);
				array[7] = array[7] + 1;
				blank = blank - 1;
				
			}
			
		else if (dif == -3)
			{
				Position mypos1;
				mypos1 = Position(posx,posy);
				movemybmp(a8, 3, mypos1);
				array[7] = array[7] + 3;
				blank = blank - 3;
				
			}
		//cout<<array[7]<<"\t"<<blank<<endl;
	}
}

float getx(int a)
{
	switch(a%3)
	{
		case 0 :
			return x[0];
		case 1 :
			return x[1];
		case 2 :
			return x[2];
		default :
			exit(0);
	}
}

float gety(int a)
{
	switch(a/3)
	{
		case 0 :
			return y[0];
		case 1 :
			return y[1];
		case 2 :
			return y[2];
		default :
			exit(0);
	}
}

