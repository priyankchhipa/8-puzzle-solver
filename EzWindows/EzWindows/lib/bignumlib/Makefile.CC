#
# bignumlib obj makefile
# Author: Jack Davidson
#


#
# The following directives let make know that you may be using files
# with a cpp extension.
#

.SUFFIXES:
.SUFFIXES: .cpp $(SUFFIXES)


#
# You may have to set the directories so that are appropiate for 
# your account.
#

CC       = CC
CHAP13   = ../../../../Student/chap13
INCLUDE  = ../../../../Student/EzWindows/include
CPPFLAGS=-I$(CHAP13)/bignum -I$(INCLUDE) -DUSING_CC

#
# The following sequence of commands defines a target for the program 
# executable.
#

numextra.o: numextra.cc
	$(CC) $(CPPFLAGS) -c numextra.cc
	cp numextra.o $(CHAP13)/bignum/bignumlib.o


#
# The target below indicates to make how to process files with a cpp 
# extension. Normally this is necessary but the cpp extension isn't 
# defined for make.
#

.cpp.o:
	$(CC) $(CPPFLAGS) -c $< 


#
# As a standard practice, a clean target is included in most make files.
# By executing 'make clean', all object files, backup files, and the 
# executable are deleted.
#

clean:
	rm -f *.o *~ 


