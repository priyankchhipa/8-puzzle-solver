Unpacking EzWindows
--------------------

1. Download the package "sampleGraphics.tgz" in your home directory. Use the command "tar -zxf sampleGraphics.tgz to unpack the package.
2. This will create three directories: samples, APIDocumentation, EzWindows.
3. The directory EzWindows contains the graphics library and header files. This must be in your home directory.
4. The directory samples contains some simple code that uses EzWindows library. Here you will see how to use the most common EzWindows functions such as for drawing lines, circles, disaplying text etc. Read all the sample programs and also compile and run them to see how they work. You should also try modifying the sample programs to better understand the functionalities.
5. The directory APIDocumentatin contains the manuals for all the functions provided by EzWindows libray.

How to compile programs to use EzWindows.
----------------------------------------
1. In any program where you want to use EzWindows instead of "int main" you will have to give "int ApiMain". Look at the sample programs for this.
2. To compile you will have to use the following command (make sure the package was untarred in you home directory)

g++ <filename> -I$HOME/EzWindows/include -I/usr/X11R6/include -L/usr/X11R6/lib -lX11 -L$HOME/EzWindows/lib -lezwin

put your program filename in place of <filename>.
3. Type ./a.out to run your program.


