#include "ezwin.h"
#include <cassert>

// Create a 10 x 4 window
SimpleWindow HelloWindow("Hello EzWindows", 10.0, 4.0, Position(5.0, 6.0));
// Try placing the above line inside ApiMain
// ApiMain(): create a window and display greeting
int ApiMain() {


	HelloWindow.Open();
	assert(HelloWindow.GetStatus() == WindowOpen);

	// Get Center of Window
	Position Center = HelloWindow.GetCenter();

	// Create bounding box for text
	Position UpperLeft = Center + Position(-1.0, -1.0);
	Position LowerRight = Center + Position(1.0,  1.0);

	// Display the text
	HelloWindow.RenderText(UpperLeft, LowerRight,
	 "Hello EzWindows", Red);
	return 0;
}
