#include "ezwin.h"
#include <cassert>

// Create a window with title "Basic", size 10cm x 10cm,
//     positioned 3cm from left, 1cm from top of screen.
SimpleWindow MyWindow("Basic", 10.0, 10.0,
		      Position(3.0, 1.0));

// ApiMain(): create a window and display some shapes
int ApiMain() {
  MyWindow.Open();
  assert(MyWindow.GetStatus() == WindowOpen);

  // Colors: {White, Red, Green, Blue, Yellow, Cyan, Magenta};

  //Green Rectangle, with no border
  MyWindow.RenderRectangle(Position(1.0,1.0), Position(2.0,2.0),
			   Green, false);

  //Yellow Rectangle, with border
  MyWindow.RenderRectangle(Position(2.5,2.5), Position(5.0,6.0),
			   Yellow, true);

  //Red Line, thickness 0
  MyWindow.RenderLine(Position(3.0,0.0),Position(3.5,2.0),
		      Red,0.0);

  //Blue Ellipse, border=true
  MyWindow.RenderEllipse( Position(1.0,6.5),Position(4.0, 8.5),
			  Blue, true);

  return 0;
} // end ApiMain()
