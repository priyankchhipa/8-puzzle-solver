#include <iostream>
#include <string>
// C Stuff
#include <cstdlib>
#include <ctime>
#include <cstdio>
using namespace std;
#include "rect.h"
#include "label.h"

int DimensionX, DimensionY;

void WrapUp(SimpleWindow &z) {
  cout << "Type Ctrl-C to remove the display and exit" << endl;
  char AnyChar;
  cin >> AnyChar;
  z.Close();
}

void generateData(int x[][2], int size) {
  for (int i = 0; i < size; ++i){
    x[i][0] =   rand() % DimensionX;
    x[i][1] =   rand() % DimensionY;
    cout << x[i][0] << x[i][0] << " ";
  }
  cout << endl;
}

void displayData(SimpleWindow &z, int ecgSignal[][2], int size) {
  for (int i = 0; i < size; ++i) {
    int x = ecgSignal[i][0];
    int y = ecgSignal[i][1];
    RectangleShape Point(z, x, y, Blue, 0.25, 0.25);
    Point.Draw();
    char mystring[80];
    sprintf(mystring, "%d", i);
    Label Item(z, x+ 0.2, y + 0.2, mystring);
    Item.Draw();
  }
}

int ApiMain() {
  srand((unsigned int) time(0));
  cout << "Enter Dimensions: (x, y) " ;
  cin >> DimensionX >> DimensionY;
  
  SimpleWindow w("My first window", DimensionX, DimensionY);
  w.Open();
  int xPoints;
  cout << "Enter number of Points: " ;
  cin >> xPoints;
  int data[xPoints][2];
  generateData(data, xPoints);
  displayData(w, data, xPoints);
  WrapUp(w);
  return 0;
}
