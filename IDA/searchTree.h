/*
 *  searchTree.h
 *  
 *
 *  Created by Devendra Chaplot on 12/10/11.
 *  Copyright 2011 IIT Bombay. All rights reserved.
 *
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>
#include <algorithm>
#include <time.h>

using namespace std;

class searchTree {
private:
	struct tree_node{
		tree_node* link1;
		tree_node* link2;
		tree_node* link3;
		tree_node* link4;
		tree_node* parent;
		vector<int> state;
		int cost;
		int index;
		int distance;
	};
	vector<int> initial, final;
	vector<tree_node*> stack;
	tree_node* root;
	int heuristic;
	int algorithm;
	bool success;
	int distanceArray[9][9];
	int fvalue;
	int check;
	int nextf;
	
public:
	searchTree() {
		root = NULL;
		nextf = 9999;
		check = 1;
	}
	
	bool isEmpty() const { return root==NULL; }
	void insert(tree_node*);
	void makeRoot(vector<int>);
	void build();
	void read();
	void makeMove(tree_node*, tree_node*, char);
	void print(tree_node*);
	void pv(tree_node*);
	void checkSuccess(tree_node*);
	int costCalculator(tree_node*,int j);
	bool checkSolvability();
	void sort();
};
