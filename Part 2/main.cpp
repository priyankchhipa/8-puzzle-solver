/*
 *  main.cpp
 *  8puzzle
 *
 *  Created by Devendra Chaplot on 12/10/11.
 *  Copyright 2011 IIT Bombay. All rights reserved.
 *
 */

#include <iostream>
#include "Puzzle.h"
using namespace std;

int main(){
	int i,b;
	int a[9];
	cout << "Enter (0 to 8 in any order)"<<endl;
	for ( i=0; i<9; i++){
		cin>>a[i];
		if (a[i] == 0) b =i;
	}
	Puzzle P;
	P.initCurrentState(a);
	P.initBlankPosition(b);
	P.printCurrentState();
	cout<< "Blank Position : "<<P.getBlankPosition() + 1<<endl;
	/*cout<<P.find(1);
	cout<<"Enter position : ";
	cin>>i;
	cout<<"Enter target : ";
	cin>>b;
	cout<<"Best Adjacent: "<<P.findBestAdjacent(i,b)<<endl;*/
	while (1) {
		cout << "Enter position to be flipped (1 to 9): ";
		cin >> b;
		P.flip(b-1);
		P.printCurrentState();
	}
	return 0;
}


