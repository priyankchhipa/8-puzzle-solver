/*
 *  Puzzle.h
 *  8puzzle
 *
 *  Created by Devendra Chaplot on 12/10/11.
 *  Copyright 2011 IIT Bombay. All rights reserved.
 *
 */

#include<iostream>
using namespace std;

class Puzzle{
private:
	int currentState[9];
	int blankPosition;
	int adjacentPosition[9][4];
	int distanceMatrix[9][9];
public:
	int find(int x);
	int flip(int y);
	void initDistanceMatrix();
	void initAdjacentPositions();
	int getAdjacent (int y,int k);
	int getBlankPosition();
	void initBlankPosition(int b);
	int findBestAdjacent(int x, int target);
	void printCurrentState();
	void initCurrentState(int a[9]);

	Puzzle (){
		initDistanceMatrix();
		initAdjacentPositions();
	}
};
