/*
 *  Puzzle.cpp
 *  8puzzle
 *
 *  Created by Devendra Chaplot on 12/10/11.
 *  Copyright 2011 IIT Bombay. All rights reserved.
 *
 */

#include<iostream>
#include "Puzzle.h"

int Puzzle :: find (int x){
	for (int i=0; i<9; i++) {
		if (currentState[i] == x) {
			return i;
		}
	}
}

int Puzzle :: flip (int y){
	currentState[blankPosition] = currentState[y];
	currentState[y] = 0;
	blankPosition = y;
}

int Puzzle :: getAdjacent (int y,int k){
	int a[4] = {-1,-1,-1,-1};
	switch (y) {
		case 0:
			a[0] = 1;
			a[1] = 3;
			return a[k];
			break;
		case 1:
			a[0] = 0;
			a[1] = 2;
			a[2] = 4;
			return a[k];
			break;
		case 2:
			a[0] = 1;
			a[1] = 5;
			return a[k];
			break;
		case 3:
			a[0] = 0;
			a[1] = 4;
			a[2] = 6;
			return a[k];
			break;
		case 4:
			a[0] = 1;
			a[1] = 3;
			a[2] = 5;
			a[3] = 7;
			return a[k];
			break;
		case 5:
			a[0] = 2;
			a[1] = 4;
			a[2] = 8;
			return a[k];
			break;
		case 6:
			a[0] = 3;
			a[1] = 7;
			return a[k];
			break;
		case 7:
			a[0] = 4;
			a[1] = 6;
			a[2] = 8;
			return a[k];
			break;
		case 8:
			a[0] = 5;
			a[1] = 7;
			return a[k];
			break;
		default:
			break;
	}
}

int Puzzle :: findBestAdjacent(int x, int target){
	int y[4];
	//cout << x << endl;
	//cout << target << endl;
	//fflush(stdout);
	y[0] = adjacentPosition[x][0];
	y[1] = adjacentPosition[x][1];
	y[2] = adjacentPosition[x][2];
	y[3] = adjacentPosition[x][3];
	//for(int i=0;i<4;i++){
	//	cout<<y[i]<<" ";
	//}
	//cout<<endl;
	int min[2];
	min[0] = y[0];
	min[1] = distanceMatrix[y[0]] [target];
	//cout << min[1]<<endl;
	fflush(stdout);
	if (distanceMatrix[y[1]][target] < min[1] && y[1] >= 0){
		min[0] = y[1];
		min[1] = distanceMatrix[y[1]] [target];
	}
	if (distanceMatrix[y[2]][target] < min[1]  && y[2] >= 0){
		min[0] = y[2];
		min[1] = distanceMatrix[y[2]] [target];
	}
	if (distanceMatrix[y[3]][target] < min[1] && y[3] >= 0){
		min[0] = y[3];
		min[1] = distanceMatrix[y[3]] [target];
	}
	return min[0];
}

void Puzzle :: initDistanceMatrix(){
	for (int i=0; i<9; i++) {
		distanceMatrix[i][i] = 0;
	}
	distanceMatrix[0][1] = 1;
	distanceMatrix[0][2] = 2;
	distanceMatrix[0][3] = 1;
	distanceMatrix[0][4] = 2;
	distanceMatrix[0][5] = 3;
	distanceMatrix[0][6] = 2;
	distanceMatrix[0][7] = 3;
	distanceMatrix[0][8] = 4;
	
	distanceMatrix[1][0] = 1;
	distanceMatrix[1][2] = 1;
	distanceMatrix[1][3] = 2;
	distanceMatrix[1][4] = 1;
	distanceMatrix[1][5] = 2;
	distanceMatrix[1][6] = 3;
	distanceMatrix[1][7] = 2;
	distanceMatrix[1][8] = 3;
	
	distanceMatrix[2][0] = 2;
	distanceMatrix[2][1] = 1;
	distanceMatrix[2][3] = 3;
	distanceMatrix[2][4] = 2;
	distanceMatrix[2][5] = 1;
	distanceMatrix[2][6] = 4;
	distanceMatrix[2][7] = 3;
	distanceMatrix[2][8] = 2;
	
	distanceMatrix[3][0] = 1;
	distanceMatrix[3][1] = 2;
	distanceMatrix[3][2] = 3;
	distanceMatrix[3][4] = 1;
	distanceMatrix[3][5] = 2;
	distanceMatrix[3][6] = 1;
	distanceMatrix[3][7] = 2;
	distanceMatrix[3][8] = 3;
	
	distanceMatrix[4][0] = 2;
	distanceMatrix[4][1] = 1;
	distanceMatrix[4][2] = 2;
	distanceMatrix[4][3] = 1;
	distanceMatrix[4][5] = 1;
	distanceMatrix[4][6] = 2;
	distanceMatrix[4][7] = 1;
	distanceMatrix[4][8] = 2;
	
	distanceMatrix[5][0] = 3;
	distanceMatrix[5][1] = 2;
	distanceMatrix[5][2] = 1;
	distanceMatrix[5][3] = 2;
	distanceMatrix[5][4] = 1;
	distanceMatrix[5][6] = 3;
	distanceMatrix[5][7] = 2;
	distanceMatrix[5][8] = 1;
	
	distanceMatrix[6][0] = 2;
	distanceMatrix[6][1] = 3;
	distanceMatrix[6][2] = 4;
	distanceMatrix[6][3] = 1;
	distanceMatrix[6][4] = 2;
	distanceMatrix[6][5] = 3;
	distanceMatrix[6][7] = 1;
	distanceMatrix[6][8] = 2;
	
	distanceMatrix[7][0] = 3;
	distanceMatrix[7][1] = 2;
	distanceMatrix[7][2] = 3;
	distanceMatrix[7][3] = 2;
	distanceMatrix[7][4] = 1;
	distanceMatrix[7][5] = 3;
	distanceMatrix[7][6] = 1;
	distanceMatrix[7][8] = 1;
	
	distanceMatrix[8][0] = 4;
	distanceMatrix[8][1] = 3;
	distanceMatrix[8][2] = 2;
	distanceMatrix[8][3] = 3;
	distanceMatrix[8][4] = 2;
	distanceMatrix[8][5] = 1;
	distanceMatrix[8][6] = 2;
	distanceMatrix[8][7] = 1;
	
}

void Puzzle :: initAdjacentPositions(){
	for (int i=0; i<9; i++) {
		adjacentPosition[i][0] = getAdjacent(i,0);
		adjacentPosition[i][1] = getAdjacent(i,1);
		adjacentPosition[i][2] = getAdjacent(i,2);
		adjacentPosition[i][3] = getAdjacent(i,3);
	}
}

void Puzzle :: printCurrentState(){
	cout << currentState[0] << "  " << currentState[1] <<"  "<<currentState[2]<<endl;
	cout << currentState[3] << "  " << currentState[4] <<"  "<<currentState[5]<<endl;
	cout << currentState[6] << "  " << currentState[7] <<"  "<<currentState[8]<<endl;
}

void Puzzle :: initCurrentState(int a[9]){
	for (int i=0; i<9; i++) {
		currentState[i] = a[i];
		//if(a[i]==0) blankPosition == i;
	}
}

int Puzzle :: getBlankPosition(){
	return blankPosition;
}

void Puzzle :: initBlankPosition(int b){
	blankPosition = b;
}


